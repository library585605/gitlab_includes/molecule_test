
# Molecule   
Для тестирование роли, нужно добавить .gitlab-ci.yml:

   Переменные.   
  Укажите в переменную готовый шаблон сценария или оставьте его пустым.   
  По умолчанию переменная:   
  MOLECULE_URL_SCENARIO: "https://gitlab.com/library585605/docker_images/molecule_scenario_1.git"
    
 Без systemd простой запуск.   
  MOLECULE_URL_SCENARIO: ""    
 

```
.gitlab-ci.yml

include:
- project: "library585605/gitlab_includes/molecule_test"
  file: "gitlab_molecule_test_roles.yml"
  ref: "main"

stages:
   - molecule_test

testing: 
  extends: .molecule_testing
  variables:
    MOLECULE_URL_SCENARIO: ""
  script:
     - molecule test
 
```
